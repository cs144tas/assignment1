import re

def parseIperf(fileName):
	nums = []
	with open(fileName, 'r') as f:
		for line in f:
			m = re.match('.*ytes[\s]+([0-9.]+)[\s]+([MK]bits).*',line)
			if m:
				num = float(m.group(1))
				if m.group(2) == "Mbits":
					num = num * 1000

				nums.append(num)

	print nums
	return nums