#!/usr/bin/python
"CS244 Spring 2015 Assignment 1: Bufferbloat"

from mininet.topo import Topo
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.net import Mininet
from mininet.log import lg, info
from mininet.util import dumpNodeConnections
from mininet.cli import CLI

from subprocess import Popen, PIPE
from time import sleep, time
from multiprocessing import Process
from argparse import ArgumentParser

from monitor import monitor_qlen, monitor_cumulative_throughput
import termcolor as T

import sys
import os
import math

parser = ArgumentParser(description="Bufferbloat tests")
parser.add_argument('--bw-host', '-B',
                    type=float,
                    help="Bandwidth of host links (Mb/s)",
                    default=1000)

parser.add_argument('--bw-net', '-b',
                    type=float,
                    help="Bandwidth of bottleneck (network) link (Mb/s)",
                    required=True)

parser.add_argument('--delay',
                    type=float,
                    help="Link propagation delay (ms)",
                    required=True)

parser.add_argument('--dir', '-d',
                    help="Directory to store outputs",
                    required=True)

parser.add_argument('--time', '-t',
                    help="Duration (sec) to run the experiment",
                    type=int,
                    default=10)

parser.add_argument('--maxq',
                    type=int,
                    help="Max buffer size of network interface in packets",
                    default=100)

parser.add_argument('--curls', '-c',
                    type=int,
                    help="Number of times to request webpage per 5 seconds",
                    default=3)

parser.add_argument('--nHosts',
                    help="Number of hosts in run",
                    type=int,
                    default=50)

parser.add_argument('--pace',
                    help="Are we using pacing",
                    type=int,
                    default=0)

# Linux uses CUBIC-TCP by default that doesn't have the usual sawtooth
# behaviour.  For those who are curious, invoke this script with
# --cong cubic and see what happens...
# sysctl -a | grep cong should list some interesting parameters.
parser.add_argument('--cong',
                    help="Congestion control algorithm to use",
                    default="reno")

# Expt parameters
args = parser.parse_args()

class BBTopo(Topo):
    "Simple topology for bufferbloat experiment."

    def build(self):
        n = args.nHosts
        #create two hosts
        hosts = []
        for i in range(n):
            hosts.append(self.addHost('h%d' % (2*i + 1), cpu = .25/n))
            hosts.append(self.addHost('h%d' % (2*i + 2), cpu = .25/n))

        #Create a switch
        switch0 = self.addSwitch('s0')
        switch1 = self.addSwitch('s1')
        self.addLink(switch0, switch1,  bw = args.bw_net,
                delay = args.delay, max_queue_size=args.maxq, use_htb=True)


        for i in range(n):
            #names are one off from the index
            self.addLink(hosts[2*i], switch0, bw = args.bw_host,
                delay = args.delay, max_queue_size=args.maxq, use_htb=True)
            self.addLink(hosts[2*i + 1], switch1, bw = args.bw_host,
                delay = args.delay, max_queue_size=args.maxq, use_htb=True)

# Simple wrappers around monitoring utilities.  You are welcome to
# contribute neatly written (using classes) monitoring scripts for
# Mininet!
def start_tcpprobe(outfile="cwnd.txt"):
    os.system("rmmod tcp_probe; modprobe tcp_probe full=1;")
    Popen("cat /proc/net/tcpprobe > %s/%s" % (args.dir, outfile),
          shell=True)

def stop_tcpprobe():
    Popen("killall -9 cat", shell=True).wait()

def start_qmon(iface, interval_sec=0.1, outfile="q.txt"):
    monitor = Process(target=monitor_qlen,
                      args=(iface, interval_sec, outfile))
    monitor.start()
    return monitor

def start_tmon(iface, interval_sec=0.1, outfile="Pacing.cthroughput"):
    monitor = Process(target=monitor_cumulative_throughput,
                      args=(iface, interval_sec, outfile))
    monitor.start()
    return monitor


#Start a simple long lived tcp flow using iperf
def start_iperf(net):
    n = args.nHosts
    for i in range(n):
        h1, h2 = net.get('h%d' % (2*i + 1), 'h%d' % (2*i + 2))
    
        #Open an iperf server to accept incoming connections
        #Use 16m so the receive window does not limit
        server2 = h2.cmd("iperf -s -w 16m &")

        #Open an iperf client connection to keep a tcp connection alive for args.time
        client1 = h1.cmd("iperf -i 1 -c %s -t %s > ./%s/iperfResults/server_output%d.txt &" % (h2.IP(), args.time, args.dir, i))



def start_webserver(net):
    h1 = net.get('h1')
    proc = h1.popen("python http/webserver.py", shell=True)
    sleep(1)
    return [proc]

#Set up ping train
def start_ping(net):
    h1, h2 = net.get("h1", "h2")

    #continuous ping from h1 to h2.
    h1.popen("ping %s -i .1 > %s/ping.txt" % (h2.IP(), args.dir), shell=True)

    pass

def runCurl(net, f):
    h1, h2 = net.get('h1', 'h2')
    curl = h2.popen("curl -o /dev/null -s -w %%{time_total} %s/http/index.html" % h1.IP())

    #get stdout from the subprocess and write it to file
    f.write(curl.communicate()[0])
    f.write("\n")


def bufferbloat():
    if not os.path.exists(args.dir):
        os.makedirs(args.dir)
    os.system("sysctl -w net.ipv4.tcp_congestion_control=%s" % args.cong)
    os.system("sysctl -w net.ipv4.tcp_pacing=%s" % args.pace)
    print "Creating Topo"
    topo = BBTopo()
    print "topology created"
    net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink)
    print "Starting Net"
    net.start()
    print "Started net"
    # This dumps the topology and how nodes are interconnected through
    # links.
    dumpNodeConnections(net.hosts)
    # This performs a basic all pairs ping test.
    #print "Pinging hosts" 
    #net.pingAll()

    # Start all the monitoring processes
    print "Starting TCP Probe"
    start_tcpprobe("cwnd.txt")

    # Start monitoring the queue sizes. Monitor the queue size on the link to h2
    print "starting qmon"
    qmon = start_qmon(iface='s0-eth1',
                      outfile='%s/q.txt' % (args.dir))

    tmon = start_tmon(iface='s0-eth1')

    #Start a long lived tcp connection between h1 and h2
    print "startin iperf"
    start_iperf(net)

    #Start a ping train between h1 and h2
    start_ping(net)
    
    h1, h2 = net.get("h1", "h2")
    
    start_time = time()

    while True:

        #Run curl some number of times then sleep for 5 seconds        
        #for i in range(args.curls):
            #runCurl(net, f)

        sleep(5)
        now = time()
        delta = now - start_time
        if delta > args.time:
            break
        print "%.1fs left..." % (args.time - delta)

    stop_tcpprobe()
    tmon.terminate()
    qmon.terminate()
    net.stop()
    print "finished"
    # Ensure that all processes you create within Mininet are killed.
    # Sometimes they require manual killing.
    Popen("pgrep -f webserver.py | xargs kill -9", shell=True).wait()

if __name__ == "__main__":
    bufferbloat()
