#!/bin/bash

# Note: Mininet must be run as root.  So invoke this shell script
# using sudo.
curl=3
time=50
bwnet=5
bwhost=1000
nHosts=1
usingPacing=0
# TODO: If you want the RTT to be 20ms what should the delay on each
# link be?  Set this value correctly.
delay=50

iperf_port=5001


# SINGLE FLOW RENO
for qsize in 25; do
    dir=bb-q$qsize
    mkdir $dir/iperfResults

    python pacing.py -b $bwnet -B $bwhost --delay $delay -d $dir --maxq $qsize --time $time -c $curl --nHosts $nHosts --pace $usingPacing #todo: arguments

    # TODO: Ensure the input file names match the ones you use in
    # bufferbloat.py script.  Also ensure the plot file names match
    # the required naming convention when submitting your tarball.
    python plot_tcpprobe.py -f $dir/cwnd.txt -o $dir/cwnd-iperf.png -p $iperf_port
    python plot_queue.py -f $dir/q.txt -o $dir/q.png
    python plot_cthroughput.py -f $dir/iperfResults/server_output0.txt -d $dir -o $dir/ct_1flow_reno.png --legend 1 --nHosts $nHosts
    #python plot_ping.py -f $dir/ping.txt -o $dir/rtt.png
done


# SINGLE FLOW PACING
usingPacing=1
for qsize in 25; do
    dir=bb-q$qsize
    mkdir $dir/iperfResults

    python pacing.py -b $bwnet -B $bwhost --delay $delay -d $dir --maxq $qsize --time $time -c $curl --nHosts $nHosts --pace $usingPacing #todo: arguments

    # TODO: Ensure the input file names match the ones you use in
    # bufferbloat.py script.  Also ensure the plot file names match
    # the required naming convention when submitting your tarball.
    python plot_tcpprobe.py -f $dir/cwnd.txt -o $dir/cwnd-iperf.png -p $iperf_port
    python plot_queue.py -f $dir/q.txt -o $dir/q.png
    python plot_cthroughput.py -f $dir/iperfResults/server_output0.txt -d $dir -o $dir/ct_1flow_pacing.png --legend 1 --nHosts $nHosts
    #python plot_ping.py -f $dir/ping.txt -o $dir/rtt.png
done


# MULTI FLOW RENO
nHosts=5
usingPacing=0
for qsize in 25; do
    dir=bb-q$qsize
    mkdir $dir/iperfResults

    python pacing.py -b $bwnet -B $bwhost --delay $delay -d $dir --maxq $qsize --time $time -c $curl --nHosts $nHosts --pace $usingPacing #todo: arguments

    # TODO: Ensure the input file names match the ones you use in
    # bufferbloat.py script.  Also ensure the plot file names match
    # the required naming convention when submitting your tarball.
    python plot_tcpprobe.py -f $dir/cwnd.txt -o $dir/cwnd-iperf.png -p $iperf_port
    python plot_queue.py -f $dir/q.txt -o $dir/q.png
    python plot_cthroughput.py -f $dir/iperfResults/server_output0.txt -d $dir -o $dir/ct_5flow_reno.png --legend 1 --nHosts $nHosts
    #python plot_ping.py -f $dir/ping.txt -o $dir/rtt.png
done


# MULTI FLOW PACING
usingPacing=1
for qsize in 25; do
    dir=bb-q$qsize
    mkdir $dir/iperfResults

    python pacing.py -b $bwnet -B $bwhost --delay $delay -d $dir --maxq $qsize --time $time -c $curl --nHosts $nHosts --pace $usingPacing #todo: arguments

    # TODO: Ensure the input file names match the ones you use in
    # bufferbloat.py script.  Also ensure the plot file names match
    # the required naming convention when submitting your tarball.
    python plot_tcpprobe.py -f $dir/cwnd.txt -o $dir/cwnd-iperf.png -p $iperf_port
    python plot_queue.py -f $dir/q.txt -o $dir/q.png
    python plot_cthroughput.py -f $dir/iperfResults/server_output0.txt -d $dir -o $dir/ct_5flow_pacing.png --legend 1 --nHosts $nHosts
    #python plot_ping.py -f $dir/ping.txt -o $dir/rtt.png
done
